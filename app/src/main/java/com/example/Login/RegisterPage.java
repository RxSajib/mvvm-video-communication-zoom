package com.example.Login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.Data.DataManager;
import com.example.PreferenceManager.PreferenceManager;
import com.example.Screen.HomePage;
import com.example.zoommvvm.R;
import com.example.zoommvvm.databinding.RegisterPageBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterPage extends AppCompatActivity {

    private RegisterPageBinding binding;
    private ProgressDialog progressDialog;
    private FirebaseFirestore MuserRef;
    private PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.register_page);

        init_view();
        registeraccount();
    }

    private void registeraccount(){
        binding.RegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(binding.FirstNameInput.getEditText().getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "First name require", Toast.LENGTH_SHORT).show();
                }else if(binding.LastNameInput.getEditText().getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Last name require", Toast.LENGTH_SHORT).show();
                }else if(binding.EmailAddressInput.getEditText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Email address require", Toast.LENGTH_SHORT).show();
                }else if(binding.PasswordInput.getEditText().getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Password require", Toast.LENGTH_SHORT).show();
                }
                else if(!binding.ConfrimPasswordInput.getEditText().getText().toString().trim().equals(binding.PasswordInput.getEditText().getText().toString().trim())){
                    Toast.makeText(getApplicationContext(), "Password wrong", Toast.LENGTH_SHORT).show();
                }
                else {
                    progressDialog.setMessage("please wait ...");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();


                    Map<String, String> usermap = new HashMap<>();
                    usermap.put(DataManager.KEY_FIRST_NAME, binding.FirstNameInput.getEditText().getText().toString().trim());
                    usermap.put(DataManager.KEY_LAST_NAME, binding.LastNameInput.getEditText().getText().toString().trim());
                    usermap.put(DataManager.KEY_EMAIL, binding.EmailAddressInput.getEditText().getText().toString().trim());
                    usermap.put(DataManager.KEY_PASSWORD, binding.PasswordInput.getEditText().getText().toString().trim());

                    MuserRef.collection(DataManager.KEY_COLLECTION_USERS)
                            .add(usermap)
                            .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentReference> task) {
                                    if(task.isSuccessful()){
                                        preferenceManager.putBoolen(DataManager.KEY_IS_SIGNED, true);
                                        preferenceManager.putString(DataManager.KEY_FIRST_NAME, binding.FirstNameInput.getEditText().getText().toString().trim());
                                        preferenceManager.putString(DataManager.KEY_LAST_NAME, binding.LastNameInput.getEditText().getText().toString().trim());
                                        preferenceManager.putString(DataManager.KEY_EMAIL, binding.EmailAddressInput.getEditText().getText().toString().trim());
                                        preferenceManager.putString(DataManager.KEY_PASSWORD, binding.PasswordInput.getEditText().getText().toString().trim());
                                        preferenceManager.putString(DataManager.KEY_USER_ID, task.getResult().getId());

                                        goto_homepage();
                                        progressDialog.dismiss();

                                    }else {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Error"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getApplicationContext(), "Error "+e
                                            .getMessage(), Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }
                            });

                }

            }
        });
    }

    private  void init_view(){
        preferenceManager = new PreferenceManager(getApplicationContext());
        MuserRef = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(RegisterPage.this);
        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void goto_homepage(){
        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}