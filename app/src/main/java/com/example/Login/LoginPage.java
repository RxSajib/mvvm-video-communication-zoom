package com.example.Login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.Data.DataManager;
import com.example.PreferenceManager.PreferenceManager;
import com.example.Screen.HomePage;
import com.example.zoommvvm.R;
import com.example.zoommvvm.databinding.LoginPageBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginPage extends AppCompatActivity {

    private LoginPageBinding binding;
    private android.app.ProgressDialog progressDialog;
    private FirebaseFirestore MFirebasefirestore;
    private PreferenceManager preferenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.login_page);

        init_view();
        login_account();
    }


    private void login_account(){
        binding.LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(binding.EmailInput.getEditText().getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Email require", Toast.LENGTH_SHORT).show();
                }else if(binding.PasswordInput.getEditText().getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Password require", Toast.LENGTH_SHORT).show();
                }
                else {
                    set_loginaccount();
                }
            }
        });
    }

    private void init_view(){
        preferenceManager = new PreferenceManager(getApplicationContext());
        if(preferenceManager.getBoolen(DataManager.KEY_IS_SIGNED)){
            goto_homepage();
        }

        MFirebasefirestore = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(LoginPage.this);
        binding.BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.RegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goto_registerpage();
            }
        });
    }

    private void goto_registerpage(){
        Intent intent = new Intent(getApplicationContext(), RegisterPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void set_loginaccount(){
        progressDialog.setMessage("please wait ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        MFirebasefirestore.collection(DataManager.KEY_COLLECTION_USERS)
                .whereEqualTo(DataManager.KEY_EMAIL, binding.EmailInput.getEditText().getText().toString().trim())
                .whereEqualTo(DataManager.KEY_PASSWORD, binding.PasswordInput.getEditText().getText().toString().trim())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful() && task.getResult().getDocuments().size() > 0){
                            DocumentSnapshot snapshot = task.getResult().getDocuments().get(0);
                            preferenceManager.putBoolen(DataManager.KEY_IS_SIGNED, true);
                            preferenceManager.putString(DataManager.KEY_FIRST_NAME, snapshot.getString(DataManager.KEY_FIRST_NAME));
                            preferenceManager.putString(DataManager.KEY_LAST_NAME, snapshot.getString(DataManager.KEY_LAST_NAME));
                            preferenceManager.putString(DataManager.KEY_EMAIL, snapshot.getString(DataManager.KEY_EMAIL));
                            preferenceManager.putString(DataManager.KEY_PASSWORD, snapshot.getString(DataManager.KEY_PASSWORD));
                            preferenceManager.putString(DataManager.KEY_USER_ID, snapshot.getId());

                            goto_homepage();
                            progressDialog.dismiss();

                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Error ", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Error login "+e
                                .getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void goto_homepage(){
        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}