package com.example.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.Data.DataManager;
import com.example.Model.Users;
import com.example.Network.ApiClint;
import com.example.Network.ApiServices;
import com.example.PreferenceManager.PreferenceManager;
import com.example.zoommvvm.R;
import com.example.zoommvvm.databinding.OutGoingMeetingInvBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.SplittableRandom;

import javax.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutGoingMeetingInv extends AppCompatActivity {

    private OutGoingMeetingInvBinding binding;
    private PreferenceManager preferenceManager;
    private String inviterToken = null;
    private String meeting_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.out_going_meeting_inv);


        init_view();
        set_statusbar_transprent();
    }

    private void init_view(){

        meeting_type = getIntent().getStringExtra("type");
        Users users = (Users) getIntent().getSerializableExtra("user");
        preferenceManager = new PreferenceManager(getApplicationContext());

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isSuccessful()){
                    inviterToken = task.getResult();
                }
            }
        });


        if(users != null && meeting_type != null){
            init_meeting(meeting_type, users.token);
        }

    }

    private void set_statusbar_transprent(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void init_meeting(String meetingtype, String receivertoken){
            try {


                JSONArray token = new JSONArray();
                token.put(receivertoken);

                JSONObject body = new JSONObject();
                JSONObject data = new JSONObject();
                data.put(DataManager.REMOTE_MSG_TYPE, DataManager.REMOTE_MSG_INVITATION);
                data.put(DataManager.REMOTE_MSG_MEETING_TYPE, meetingtype);
                data.put(DataManager.KEY_FIRST_NAME, preferenceManager.getString(DataManager.KEY_FIRST_NAME));
                data.put(DataManager.KEY_LAST_NAME, preferenceManager.getString(DataManager.KEY_LAST_NAME));
                data.put(DataManager.KEY_EMAIL, preferenceManager.getString(DataManager.KEY_EMAIL));
                data.put(DataManager.REMOTE_MSG_INVITER_TOKEN, inviterToken);

                body.put(DataManager.REMOTE_MSG_DATA, data);
                body.put(DataManager.REMOTE_MGS_REGISTERTION_IDS, token);

                send_remotemessage(body.toString(), DataManager.REMOTE_MSG_INVITATION);
            }
            catch (Exception e){
                Toast.makeText(getApplicationContext(), e
                        .getMessage(), Toast.LENGTH_SHORT).show();

                Log.d("error", e.getMessage());
            }

    }

    private void send_remotemessage(String remotemessage_body, String type){
        new ApiClint().getRetrofit().create(ApiServices.class)
                .sendRemotemessage(DataManager.getremotemessageHeader(), remotemessage_body)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(@Nullable Call<String> call,@Nullable Response<String> response) {
                        if(response.isSuccessful()){
                            if(type.equals(DataManager.REMOTE_MSG_INVITATION)){
                                Toast.makeText(getApplicationContext(), "invitation send success", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getApplicationContext(), String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<String> call,@Nullable Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error API CALL", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

    }
}