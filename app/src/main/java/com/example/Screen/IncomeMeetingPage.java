package com.example.Screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.zoommvvm.R;
import com.example.zoommvvm.databinding.IncomeMeetingPageBinding;

public class IncomeMeetingPage extends AppCompatActivity {

    private IncomeMeetingPageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.income_meeting_page);

        set_statusbar_transprent();
    }

    private void set_statusbar_transprent(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}