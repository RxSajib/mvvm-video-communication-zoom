package com.example.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.Adapter.UserAdapter;
import com.example.Data.DataManager;
import com.example.Lisiners.UserLisiners;
import com.example.Login.LoginPage;
import com.example.Model.Users;
import com.example.PreferenceManager.PreferenceManager;
import com.example.zoommvvm.R;
import com.example.zoommvvm.databinding.ActivityHomePageBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.internal.FirebaseInstanceIdInternal;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomePage extends AppCompatActivity implements UserLisiners {

    private ActivityHomePageBinding binding;
    private PreferenceManager preferenceManager;
    private List<Users> userslist = new ArrayList<>();
    private UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_page);

        init_view();
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isSuccessful()){
                    send_fcm_tokendatabase(task.getResult());
                }
            }
        });

        binding.SwapeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getuser();
            }
        });


        getuser();
    }

    private void getuser(){
        binding.SwapeRefreshLayout.setRefreshing(true);
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        database.collection(DataManager.KEY_COLLECTION_USERS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        String userid = preferenceManager.getString(DataManager.KEY_USER_ID);
                        if(task.isSuccessful()){
                            userslist.clear();
                            binding.SwapeRefreshLayout.setRefreshing(false);
                            for(QueryDocumentSnapshot snapshot : task.getResult()){
                                if(userid.equals(snapshot.getId())){
                                    continue;
                                }
                                    Users users = new Users();
                                    users.first_name = snapshot.getString(DataManager.KEY_FIRST_NAME);
                                    users.last_name = snapshot.getString(DataManager.KEY_LAST_NAME);
                                    users.email = snapshot.getString(DataManager.KEY_EMAIL);
                                    users.token = snapshot.getString(DataManager.KEY_FCM_TOKEN);
                                    userslist.add(users);

                                if(userslist.size() > 0){
                                    userAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                });
    }

    private void init_view(){
        userAdapter = new UserAdapter(userslist, this);
        binding.RecylerviewID.setHasFixedSize(true);
        binding.RecylerviewID.setAdapter(userAdapter);

        preferenceManager = new PreferenceManager(getApplicationContext());
        binding.UserName.setText(String.format(
                "%s %s",
                preferenceManager.getString(DataManager.KEY_FIRST_NAME),
                preferenceManager.getString(DataManager.KEY_LAST_NAME)
        ));

        binding.LogOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseFirestore database = FirebaseFirestore.getInstance();
                DocumentReference documentReference =   database.collection(DataManager.KEY_COLLECTION_USERS)
                        .document(preferenceManager.getString(DataManager.KEY_USER_ID));

                HashMap<String, Object> updatemap = new HashMap<>();
                updatemap.put(DataManager.KEY_FCM_TOKEN, FieldValue.delete());
                documentReference.update(updatemap)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    preferenceManager.clearpreference();
                                    Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    private void send_fcm_tokendatabase(String token){
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        DocumentReference documentReference =   database.collection(DataManager.KEY_COLLECTION_USERS)
                .document(preferenceManager.getString(DataManager.KEY_USER_ID));

        documentReference.update(DataManager.KEY_FCM_TOKEN, token)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){

                        }else {
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void init_video_meeting(Users users) {
        if(users.token == null || users.token.trim().isEmpty()){
            Toast.makeText(getApplicationContext(), "User is not active for meeting", Toast.LENGTH_SHORT).show();
        }
        else {
            Intent intent = new Intent(getApplicationContext(), OutGoingMeetingInv.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("type", "video");
            intent.putExtra("user", users);
            startActivity(intent);
        }
    }

    @Override
    public void init_Audio_meeting(Users users) {
        if(users.token == null || users.token.trim().isEmpty()){
            Toast.makeText(getApplicationContext(), "User is active"+users.first_name, Toast.LENGTH_SHORT).show();
        }
        else {

        }
    }
}