package com.example.Data;

import java.util.HashMap;

public class DataManager {

    public static final String KEY_COLLECTION_USERS = "users";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";

    public static final String KEY_PREFERENCE_NAME = "video_meeting";
    public static final String KEY_IS_SIGNED = "is_signed";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_FCM_TOKEN = "token";

    public static final String BASE_URL = "https://fcm.googleapis.com/fcm/";

    public static final String REMOTE_MSG_TYPE = "type";
    public static final String REMOTE_MSG_INVITATION = "iniviting_type";
    public static final String REMOTE_MSG_INVITER_TOKEN = "inviter_token";
    public static final String REMOTE_MSG_DATA = "data";
    public static final String REMOTE_MGS_REGISTERTION_IDS = "registration_ids";
    public static final String REMOTE_MSG_MEETING_TYPE = "meeting_type";

    public static final String REMOTE_MSG_AUTHORIZATION = "Authorization";
    public static final String REMOTE_MSG_CONTENT_TYPE = "Content-Type";

    public static HashMap<String, String> getremotemessageHeader(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(DataManager.REMOTE_MSG_AUTHORIZATION, "key=AAAAXq8MT5Y:APA91bF8UnmSdWTMOOUq4zSVgLXX_CjcNrfA9r0-VbllP4k2diEFPkVUGqq6U5lzhvTlKPiKGeS-jFW9UGTd4qEM8AbJv58Sofon5KdXpYS7WrdmiD5K8ZwhAjEOCzALkpvM8bqb9Atu");

        headers.put(DataManager.REMOTE_MSG_CONTENT_TYPE, "application/json");
        return headers;
    }
}
