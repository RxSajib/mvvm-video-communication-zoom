package com.example.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Lisiners.UserLisiners;
import com.example.Model.Users;
import com.example.zoommvvm.R;
import com.google.firebase.firestore.auth.User;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private List<Users> userList;
    private UserLisiners userLisiners;

    public UserAdapter(List<Users> userList, UserLisiners userLisiners) {
        this.userList = userList;
        this.userLisiners = userLisiners;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.user_single_layout, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.setuserdata(userList.get(position));


    }

    @Override
    public int getItemCount() {
        if(userList == null){
            return 0;
        }else {
            return userList.size();
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder{

        private TextView Username, Emailaddress, userchar_name;
        private ImageView AudioCall, VideoCall;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);

            Username = itemView.findViewById(R.id.Username);
            Emailaddress = itemView.findViewById(R.id.UserEmail);
            AudioCall = itemView.findViewById(R.id.AudioCall);
            VideoCall = itemView.findViewById(R.id.VideCall);
            userchar_name = itemView.findViewById(R.id.UserCharName);
        }

        void setuserdata(Users user){
            Username.setText(user.first_name+" "+user.last_name);
            Emailaddress.setText(user.email);
            userchar_name.setText(String.format("%s", user.first_name.substring(0,2)));


            AudioCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userLisiners.init_Audio_meeting(user);
                }
            });

            VideoCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userLisiners.init_video_meeting(user);
                }
            });
        }
    }
}
