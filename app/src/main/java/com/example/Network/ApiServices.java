package com.example.Network;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface ApiServices {

    @POST("send")
    Call<String> sendRemotemessage(
            @HeaderMap HashMap<String, String> headermap,
            @Body String body
    );
}
